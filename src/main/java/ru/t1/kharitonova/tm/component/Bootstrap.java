package ru.t1.kharitonova.tm.component;

import ru.t1.kharitonova.tm.api.ICommandController;
import ru.t1.kharitonova.tm.api.ICommandRepository;
import ru.t1.kharitonova.tm.api.ICommandService;
import ru.t1.kharitonova.tm.constant.ArgumentConst;
import ru.t1.kharitonova.tm.constant.CommandConst;
import ru.t1.kharitonova.tm.controller.CommandController;
import ru.t1.kharitonova.tm.model.Command;
import ru.t1.kharitonova.tm.repository.CommandRepository;
import ru.t1.kharitonova.tm.service.CommandService;
import ru.t1.kharitonova.tm.util.FormatUtil;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    public void start(String[] args) {
        runArguments(args);
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:\n");
            final String command = scanner.nextLine();
            runCommand(command);
        }
    }

    private void runArguments(final String[] arg){
        if(arg == null || arg.length == 0) return;
        for (String s : arg) {
            runArgument(s);
        }
        System.exit(0);
    }

    private void runCommand(final String param){
        switch (param) {
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void runArgument(final String param){
        switch (param) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
        }
    }

    public void exit() {
        System.exit(0);
    }

}
